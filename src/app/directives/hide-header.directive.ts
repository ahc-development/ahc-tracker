import { Directive, Input, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[hide-header]',
  host: {
    '(ionScroll)': 'onContentScroll($event)'
  }
})
export class HideHeaderDirective {

  @Input('header') header: any;
  headerHeight;
  scrollContent;

  constructor(
    public element: ElementRef,
    public renderer: Renderer
  ) {
    this.element.nativeElement.scrollEvents = true;
  }

  ngOnInit() {
    this.header.el.classList.add('hidden');
    this.header.el.classList.add('animatable');
  }

  onContentScroll(event) {
    if (event.detail.scrollTop > 100) {
      this.header.el.classList.remove('hidden');
    } else {
      this.header.el.classList.add('hidden');
    }
  }

}
