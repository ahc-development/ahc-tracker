import { Component } from '@angular/core';
import { EventsModalPage } from '../events/events-modal/events-modal.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  constructor(public modalController: ModalController) {}

  async openAddEvent() {
    const modal = await this.modalController.create({
      component: EventsModalPage,
      componentProps: {}
    });
    return await modal.present();
  }
}
