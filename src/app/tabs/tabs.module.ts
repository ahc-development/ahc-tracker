import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';

import { EventsModalPageModule } from '../events/events-modal/events-modal.module';
import { EventsModalPage } from '../events/events-modal/events-modal.page';

@NgModule({
  entryComponents: [EventsModalPage],
  imports: [
    EventsModalPageModule,
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
