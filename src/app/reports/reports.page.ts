import { Component, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage {

  @ViewChild('barChart', {static: false}) barChart;
  @ViewChild('pieChart', {static: false}) pieChart;
  @ViewChild('lineChart', {static: false}) lineChart;

  constructor() { }

  ionViewDidEnter() {
    this.createBarChart();
    this.createPieChart();
    this.createLineChart();
  }

  createBarChart() {
    new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
          label: 'Number of episodens',
          data: [3, 3, 6, 7, 10, 12, 10, 11, 10, 9, 5, 4],
          backgroundColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderWidth: 0
        }]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  createLineChart() {
    new Chart(this.lineChart.nativeElement, {
      type: 'line',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [
          {
          label: 'Flinarizine',
          data: [3, 3, 3, 3, 3, 3.5, 3.5, 3.5, 4, 4, 4, 4],
          backgroundColor: 'rgb(255, 194, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(255, 194, 129)', // array should have same number of elements as number of dataset
          yAxisID: 'y-axis-1',
          fill: false
        },
          {
          label: 'Number of Episodes',
          data: [3, 3, 6, 7, 10, 12, 10, 11, 10, 9, 5, 4],
          backgroundColor: 'rgb(38, 0, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(38, 0, 129)', // array should have same number of elements as number of dataset
          yAxisID: 'y-axis-2',
          fill: false
        }
        ]
      },
      options: {
        legend: {
          labels: {
            boxWidth: 10,
            borderWidth: 0
          }
        },
        scales: {
          yAxes: [{
            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
            display: true,
            position: 'left',
            id: 'y-axis-2'
          }, {
            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
            display: true,
            position: 'right',
            id: 'y-axis-1',
            gridLines: {
              drawOnChartArea: false, // only want the grid lines for one axis to show up
            },
          }],
        }
      }
    });
  }

  createPieChart() {
    new Chart(this.pieChart.nativeElement, {
      type: 'pie',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
          label: 'Number of episodens',
          data: [3, 3, 6, 7, 10, 12, 10, 11, 10, 9, 5, 4],

          backgroundColor: [
            '#86423B',
            '#974B58',
            '#9F5A79',
            '#9A6E99',
            '#8785B4',
            '#679DC6',
            '#3CB3CB',
            '#14C7C2',
            '#3BD8AE',
            '#74E693',
            '#AEF077',
            '#EBF563'
          ],
          borderWidth: 0
        }]
      },
      options: {
        showAllTooltips: true,
        legend: {
          display: true,
          position: 'right',
          labels: {
            boxWidth: 10
          }
        }
      }
    });
  }
}
