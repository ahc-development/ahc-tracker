import { Component, OnInit } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public appPages = [
    {
      title: 'Events',
      url: '/home',
      icon: 'recording'
    },
    {
      title: 'Profiles',
      url: '/profiles',
      icon: 'people'
    },
    {
      title: 'Setting',
      url: '/settings',
      icon: 'settings'
    },
    {
      title: 'Logout',
      url: '/index',
      icon: 'log-out'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuCtrl: MenuController,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.router.events.subscribe((event: RouterEvent) => {
      if (
        event instanceof NavigationEnd
          && (
            event.url === '/register'
              || event.url === '/login'
              || event.url === '/index'
              || event.url === '/'
          )
      ) {
        this.menuCtrl.enable(false);
      } else {
        // this.menuCtrl.enable(true);
      }
    });
  }
}
