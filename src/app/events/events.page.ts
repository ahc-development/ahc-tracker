import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { ModalController } from '@ionic/angular';
import { EventsDetailsModalPage } from './events-details-modal/events-details-modal.page';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss']
})
export class EventsPage implements OnInit {
  collapseCard = false;

  today = new Date();

  eventSource = [
    {
      title: '',
      severity: 1,
      startTime: new Date(new Date().setDate(new Date().getDate() - 0)),
      endTime: new Date(new Date().setDate(new Date().getDate() - 0)),
      allDay: true
    },
    {
      title: '',
      severity: 2,
      startTime: new Date(new Date().setDate(new Date().getDate() - 1)),
      endTime: new Date(new Date().setDate(new Date().getDate() - 1)),
      allDay: true
    },
    {
      title: '',
      severity: 3,
      startTime: new Date(new Date().setDate(new Date().getDate() - 2)),
      endTime: new Date(new Date().setDate(new Date().getDate() - 2)),
      allDay: true
    },
    {
      title: '',
      severity: 4,
      startTime: new Date(new Date().setDate(new Date().getDate() - 3)),
      endTime: new Date(new Date().setDate(new Date().getDate() - 3)),
      allDay: true
    },
    {
      title: '',
      severity: 5,
      startTime: new Date(new Date().setDate(new Date().getDate() - 4)),
      endTime: new Date(new Date().setDate(new Date().getDate() - 4)),
      allDay: true
    }
  ];
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  event = {
    title: 'Daily report',
    severity: 1,
    startTime: new Date(),
    endTime: new Date(),
    allDay: true
  };

  minDate = new Date().toISOString();
  maxDate = new Date().toISOString();

  monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  calendarTitle = '';

  @ViewChild(CalendarComponent, { static: true }) myCalendar: CalendarComponent;

  constructor(public modalController: ModalController) {}

  addEvent() {
    this.myCalendar.loadEvents();
  }

  async eventClicked(event: any) {
    const modal = await this.modalController.create({
      component: EventsDetailsModalPage,
      componentProps: event
    });
    return await modal.present();
  }

  prevMonth() {
    this.myCalendar.slidePrev();
  }

  nextMonth() {
    this.myCalendar.slideNext();
  }

  // (onCurrentDateChanged)="onCurrentDateChanged($event)"
  onCurrentDateChanged(event: any) {
    this.calendarTitle = `${
      this.monthNames[event.getMonth()]
    }, ${event.getFullYear()}`;
  }

  ngOnInit() {
    this.addEvent();
  }
}
