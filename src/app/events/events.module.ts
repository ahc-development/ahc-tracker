import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventsPage } from './events.page';
import { SharedModule } from '../shared/shared.module';

import { NgCalendarModule } from 'ionic2-calendar';

import { EventsDetailsModalPageModule } from './events-details-modal/events-details-modal.module';
import { EventsDetailsModalPage } from './events-details-modal/events-details-modal.page';

const routes: Routes = [
  {
    path: '',
    component: EventsPage
  }
];

@NgModule({
  entryComponents: [EventsDetailsModalPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgCalendarModule,
    EventsDetailsModalPageModule
  ],
  declarations: [EventsPage]
})
export class EventsPageModule {}
