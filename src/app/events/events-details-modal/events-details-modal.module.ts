import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventsDetailsModalPage } from './events-details-modal.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [EventsDetailsModalPage]
})
export class EventsDetailsModalPageModule {}
