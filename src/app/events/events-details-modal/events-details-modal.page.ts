import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-events-details-modal',
  templateUrl: './events-details-modal.page.html',
  styleUrls: ['./events-details-modal.page.scss']
})
export class EventsDetailsModalPage implements OnInit {
  event: any;
  date: Date;
  monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  constructor(
    public navParams: NavParams,
    public modalController: ModalController
  ) {
    this.event = navParams.get('events')[0];
    this.date = navParams.get('date');
    // console.log(this.event);
  }

  public dismiss() {
    this.modalController.dismiss();
  }

  ngOnInit() {}
}
