import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-events-modal',
  templateUrl: './events-modal.page.html',
  styleUrls: ['./events-modal.page.scss']
})
export class EventsModalPage {
  public date = new Date().toISOString();
  public severity = 1;
  public sliderColor = 'severe-1';

  constructor(public modalController: ModalController) {}

  dismiss() {
    this.modalController.dismiss();
  }

  save() {
    this.modalController.dismiss();
  }

  onRangeChangeHandler() {
    this.sliderColor = `severe-${this.severity}`;
  }
}
