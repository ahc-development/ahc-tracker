import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  selectedElement = 1;

  constructor() { }

  ngOnInit() {
  }

  scroll(el, index) {
    this.selectedElement = index;
    el.scrollIntoView();
  }
}
